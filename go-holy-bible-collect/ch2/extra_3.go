package main

import "fmt"

func f2()  {
}


var g = "g"

func main()  {

	f2 := "f2"
	fmt.Println(f2) //"f2"; local var f shadows package-level func f
	fmt.Println(g) //""g; package-level var
	//fmt.Println(h) //compile error: undefined: h

}