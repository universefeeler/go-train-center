package main

import (
	"flag"
	"fmt"
	"strings"
)

// pointer is key technology for realize falg
var n = flag.Bool("n", false, "omit trailing newline")
var sep = flag.String("s", " ", "separator")

func main() {
	flag.Parse()
	fmt.Print(strings.Join(flag.Args(), *sep))
	if !*n {
		fmt.Println()
	}
}

//➜  ch2 git:(master) ✗ go run echo4.go a bc def
//a bc def
//➜  ch2 git:(master) ✗ go run echo4.go -s / a bc def
//a/bc/def
//➜  ch2 git:(master) ✗ go run echo4.go -n a bc def
//a bc def%                                                                                                                                    ➜  ch2 git:(master) ✗ go run echo4.go -help
//Usage of /var/folders/yq/gzptf7bj5zv09crl9rtq2hsm0000gn/T/go-build385978666/b001/exe/echo4:
//-n    omit trailing newline
//-s string
//separator (default " ")
//exit status 2

