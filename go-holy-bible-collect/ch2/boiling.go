package main

import "fmt"

//four kind of type declar var const type func
const boilingF  = 212.0

func main() {
	var f = boilingF
	var c = (f - 32) * 5 / 9
	fmt.Printf("boiling point = %gF or %g C\n", f, c)
}

//➜  ch2 git:(master) ✗ go run boiling.go
//boiling point = 212F or 100 C
