package main

import "fmt"

func f() *int {

	v := 1
	return &v

}

func incr(p *int) int {
	//just incr value of p refer, not change pointer
	*p++
	return *p

}

func main()  {
	var x,y int
	fmt.Println(&x == &x, &x == &y, &x == nil)

	//p still reference variable v
	var p = f()
	fmt.Println(p)
	fmt.Println(*p)
	//return different result each call f()
	fmt.Println(f() == f())

	v := 1
	//v now is 2
	incr(&v)
	//v is now 3
	fmt.Println(incr(&v))


}

//true false false
//0xc0000a4020
//1
//false
//3
