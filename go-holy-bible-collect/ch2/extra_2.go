package main


var global *int

//variable x is allocated to heap， have escape， need extra memory
func f1() {
	var x int
	x = 1
	global = &x
}

//variable y is allocated to stack, no escape
func g() {
	y := new(int)
	*y = 1
}

func main()  {

	f1()
	g()

}
