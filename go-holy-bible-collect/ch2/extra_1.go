package main

import "fmt"

func newInt() *int {
	return new(int)
}

//func newInt() *int {
//	var dummy int
//	return &dummy
//}


func main()  {

	//new is function, not keyword
	p := new(int)
	fmt.Println(*p)

	*p =2
	fmt.Println(*p)

	//false
	q := new(int)
	fmt.Println(p == q)

}
