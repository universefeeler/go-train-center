package main

import "fmt"

func main () {
	const freezing, boilingF  =  32.0, 212.0
	fmt.Printf("%gF = %gC\n", freezing, fToc(freezing))
	fmt.Printf("%gF = %gC\n", boilingF, fToc(boilingF))

}
func fToc(f float64) float64 {
	return (f - 32) * 5 / 9
}


//➜  ch2 git:(master) ✗ go run ftoc.go
//32F = 0C
//212F = 100C
