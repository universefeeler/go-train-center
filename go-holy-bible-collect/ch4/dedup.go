package main

import (
	"bufio"
	"os"
	"fmt"
)


// 是mac，在idea里面运行的，control+D是终止输入, 也就是EOF
//文件的是直接 cat input | go run main.go

func main()  {

	seen := make(map[string]bool)
	input := bufio.NewScanner(os.Stdin)
	// put Scan on for circle it not good method
	for input.Scan() {
		if input.Text() == "end" { break }
		line := input.Text()
		if !seen[line] {
			seen[line] = true
			fmt.Println(line)
		}
	}

	if err := input.Err(); err != nil {
		fmt.Fprintf(os.Stderr, "dedup: %v\n", err)
		os.Exit(1)
	}

}
