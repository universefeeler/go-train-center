package main

import (
	"crypto/sha256"
	"fmt"
)

//there are four kinds of complex type: array, slice, map, struct
//array and struct is compound type; array have the same type, struct have different type, array and struct size is  fixed，slice and map id dynamic struct
func main() {
	c1 := sha256.Sum256([]byte("x"))
	c2 := sha256.Sum256([]byte("X"))
	fmt.Printf("%x\n%x\n%t\n%T\n", c1, c2, c1 == c2, c1)
}


//2d711642b726b04401627ca9fbac32f5c8530fb1903cc4db02258717921a4881
//4b68ab3847feda7d6c62c1fbcbeebfa35eab7351ed5e78f4ddadea5df64b8015
//false
//[32]uint8