package main

import (
	"html/template"
	"time"
)

const templ = `{{.TotalCount}} issues：
	{{range .Items}}-----------------------
	Number: {{.Number}}
	User：{{.User.Login}}
	Title：{{.Title | printf "%.64s"}}
	Age: {{.CreateAt | daysAgo }} days 
    {{end}}`

func daysAgo(t time.Time)  int {

	return int(time.Since(t).Hours() / 24)

}

var issueList = template.Must(template.New("issuelist").Parse(
	`
	<h1>{{.TotalCount}} issues</h1>
	<table>
		<tr style='text-align: left'>
			<th>#</th>
			<th>State</th>
			<th>User</th>
			<th>Title</th>
		</tr>
		{{range .Items}}
		<tr>
			<td><a href='{{.HTMLURL}}'>{{.Number}}</a></td>
			<td>{{.State}}</td>
			<td><a href='{{.User.HTMLURL}}'>{{.User.Login}}</a></td>
			<td><a href='{{.HTMLURL}}'>{{.Title}}</a></td>	
		</tr>
	</table>
	`))

var report = template.Must(template.New("issuelist").
			Funcs(template.FuncMap{"daysAgo": daysAgo}).
			Parse(templ))


//func main()  {
//
//	result, err := SearchIssues(os.Args[1:])
//	if err != nil {
//		log.Fatal(err)
//	}
//	if err := report.Execute(os.Stdout, result); err != nil {
//		log.Fatal(err)
//	}
//
//}

func main()  {

	result, err := SearchIssues(os.Args[1:])
	if err != nil {
		log.Fatal(err)
	}
	if err := issueList.Execute(os.Stdout, result); err != nil {
		log.Fatal(err)
	}

}



//➜  ch4 git:(master) ✗ go run issuesreport.go github.go repo:golang/go

// ch4 git:(master) ✗ go run issuesreport.go github.go repo:golang/go > issues.html
