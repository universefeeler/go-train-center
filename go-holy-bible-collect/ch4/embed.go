package main

import "fmt"


type Circle struct{
	Center Point
	Radius int
}

type Wheel struct {
	Circle Circle
	Spokes int
}

type Point struct {
	X, Y int
}

// struct and anonymous
func main()  {
	var w Wheel
	w.Circle.Center.X = 8
	w.Circle.Center.Y = 8
	w.Circle.Radius = 5
	w.Spokes = 20

	// not ok
	//w = Wheel{
	//	Circle: Circle{
	//		Point: Point{X: 8, Y: 8},
	//		Radius: 5,
	//	},
	//	Spokes: 20,
	//}

	w = Wheel{Circle{Point{X: 8, Y: 8}, 5,}, 20,}

	fmt.Printf("%#v\n", w)

	//not ok
	//w.X = 42

	fmt.Printf("%#v\n", w)

}


