package main

import (
	"time"
	"net/url"
	"strings"
	"net/http"
	"fmt"
	"encoding/json"
)

const IssuesURL  =  "https://api.github.com/search/issues"

type IssuesSearchResult struct{
	TotalCount int `json:"total_count"`
	Items []*Issue
}

type Issue struct {

	Number int
	HTMLURL string `json:"html_url"`
	Title string
	Statle string
	User *User
	CreateAt time.Time `json:"created_at"`
	Body string //in Markdown format

}

type User struct {
	Login string
	HTMLURL string `json:"html_url"`
}


func SearchIssues(terms []string)(*IssuesSearchResult, error) {
	q := url.QueryEscape(strings.Join(terms, " "))
	resp, err := http.Get(IssuesURL + "?q=" + q)
	if err != nil {
		return nil, err
	}

	//we must close resp.body on all execution paths.
	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
		return nil, fmt.Errorf("esarch query failed: %s", resp.Status)
	}

	var result IssuesSearchResult
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		resp.Body.Close()
		return nil, err
	}
	resp.Body.Close()
	return &result, nil
}

//func main()  {
//
//	result, err := SearchIssues(os.Args[1:])
//	if err != nil {
//		log.Fatal(err)
//	}
//	fmt.Printf("%d issues:\n", result.TotalCount)
//	for _, item := range result.Items {
//		fmt.Printf("#%-5d %9.9s %.55s\n", item.Number, item.User.Login, item.Title)
//	}
//
//}

//  ch4 git:(master) ✗ go run github.go repo:golang/go
//37089 issues:
//#37313 absolute8 fatal: morestack on g0 while running a server for half-
//#37312 griesemer x/tools/go/internal/gcimporter: remove support for bina
//#37311    eudore net/http/pprof: not set Index func response header cont
//#37310  bradfitz cmd/link: unexpected fault address (when low on disk sp
//#37309 nickgerac x/website:
//#37308 marwan-at encoding/json: json.Number breaks behavior in Go 1.14
//#37307 josharian net/http: TestDontCacheBrokenHTTP2Conn flakes
//#37306 josharian cmd/go: TestExecutableGOROOT flake
//#37305 josharian cmd/go: TestCPUProfileMultithreaded flake
//#37304 james-joh go test JSON output reports failure if stdout redirecte
//#37303 embeddedg proposal: Immutable data
//#37302 abuchanan proposal: Go 2: permit implicit conversion of constants
//#37301   perillo cmd/go: 'go help get' should not document the -v flag
//#37300   perillo cmd/go: go list inconsistently resolves empty paths to
//#37299      ohir cmd/gofmt: proposal: Minimize adjacent whitespace chang
//#37298   cespare cmd/compile: -d=checkptr false positive(?) with unalign
//#37297    gonyyi Iterate go routine with an anonymous function
//#37296 ws7789337 Sandbox Process
//#37295    erifan gollvm: find a better way to deal with g
//#37294 catenacyb crypto/elliptic: invalid handling for infinity point
//#37293 Abhaykuma Values of millisecond lost when using Mongo Driver to r
//#37292 jonperrym proposal: Go 2: integrated compile time language
//#37291     fishy go.dev: example code should be displayed as package mai
//#37290     fzipp x/exp/ebnf: support raw strings as used by EBNF in the
//#37289 GrigoriyM cmd/go: escape $WORK in ccompile when -n is set
//#37288 paleozogt cmd/go: gccgo crashes when building json-iterator
//#37287 edoardott x/tour: translate error
//#37286    spellr plugin: Open with cgo panics: runtime: no plugin module
//#37285    artyom x/tools/gopls: adds import for package not required by
//#37284 fxamacker go.dev: README tables are not rendered properly (no lin



