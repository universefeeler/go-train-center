package main

import (
	"net"
	"fmt"
)

func main() {
	var v net.Flags = net.FlagMulticast | net.FlagUp
	fmt.Print("%b %t\n", v, IsUp(v))
	TurnDown(&v)
	fmt.Printf("%b %t\n", v, IsUp(v))
	SetBroadcast(&v)
	fmt.Printf("%b %t\n", v, IsUp(v))
	fmt.Printf("%b %t\n", v, IsCast(v))

}
func IsCast(v net.Flags) interface{} {
	return v&(net.FlagBroadcast|net.FlagMulticast) != 0

}
func SetBroadcast(v *net.Flags) {
	*v |= net.FlagBroadcast
}
func TurnDown(v *net.Flags) {
	*v &^= net.FlagUp
}

func IsUp(v net.Flags) bool {

	return v&net.FlagUp == net.FlagUp

}
