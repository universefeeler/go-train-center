package main

import (
	"bufio"
	"os"
	"fmt"
)

func main()  {
	counts := make(map[string]int)
	input := bufio.NewScanner(os.Stdin)
	for input.Scan() {
		counts[input.Text()]++
	}

	for line,n := range counts {
		if n > 1 {
			fmt.Printf("%d\t%s\n", n, line)
		}
	}

}

//note: need ctrl + d，means EOF end input
//➜  ch1 git:(master) ✗ go run dup1.go
//sed
//cdv
//sed
//cdss
//cscd
//sed
//sed
//cds
//
//
//
//
//
//3D
//2
//4       sed