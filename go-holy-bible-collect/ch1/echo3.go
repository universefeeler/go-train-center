package main

import (
	"fmt"
	"strings"
	"os"
)

func main() {
	fmt.Println(strings.Join(os.Args[1:], "#"))
}

//➜  ch1 git:(master) ✗ go run echo2.go 1 2 3 4 5 6
//1#2#3#4#5#6
