//Echo prints its command-line arguments
package main

import (
	"os"
	"fmt"
)

func main() {
	var s, sep string
	fmt.Println(os.Args[0]) //
	for i := 1; i < len(os.Args); i++ {
		fmt.Println(os.Args[i])
		s += sep + os.Args[i]
		sep = " "
	}
	fmt.Println(s)
}

//➜  ch1 git:(master) ✗ go run echo1.go 1 2 3 4 5 6
///var/folders/yq/gzptf7bj5zv09crl9rtq2hsm0000gn/T/go-build482591875/b001/exe/echo1
//1
//2
//3
//4
//5
//6
//1 2 3 4 5 6
//➜  ch1 git:(master) ✗ go run echo1.go 1,2,3,4,5,6
///var/folders/yq/gzptf7bj5zv09crl9rtq2hsm0000gn/T/go-build482591875/b001/exe/echo1
//1,2,3,4,5,6
//1,2,3,4,5,6
