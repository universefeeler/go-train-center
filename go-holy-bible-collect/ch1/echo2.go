package main

import (
	"os"
	"fmt"
)

func main() {

	s, sep := "",""
	for _,arg := range os.Args[1:] {
		s += sep +arg
		sep = "#"
	}
	fmt.Println(s)

}

//➜  ch1 git:(master) ✗ go run echo2.go 1 2 3 4 5 6
//1#2#3#4#5#6

