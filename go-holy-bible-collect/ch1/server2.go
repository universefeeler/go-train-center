package main

import (
	"net/http"
	"fmt"
	"log"
	"sync"
)


var mu sync.Mutex
var count int

// server handle every request with a new goroutine, so can handle many requeat at the same time
func main() {
	http.HandleFunc("/", handler2)
	http.HandleFunc("/count", counter)
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}

func handler2(w http.ResponseWriter, r *http.Request) {
	//if not lock, then bug
    mu.Lock()
    count++
    mu.Unlock()
	fmt.Fprintf(w, "URL.Path = %q\n", r.URL.Path)

}


func counter(w http.ResponseWriter, r *http.Request) {
    mu.Lock()
	fmt.Fprintf(w, "Count %d\n", count)
    mu.Unlock()

}
