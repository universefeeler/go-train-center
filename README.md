
## Getting Started go-train-center
### Reference
#### Base Reference
1. [Go 基础](https://blog.csdn.net/k346k346/category_9289543.html)
2. [Go语言学习](https://blog.csdn.net/vigiking/category_8962885.html)
3. [Go开发](https://www.cnblogs.com/Survivalist/category/1350741.html)
4. [Go区块链开发](https://www.cnblogs.com/Survivalist/tag/%E5%8C%BA%E5%9D%97%E9%93%BE%E5%BC%80%E5%8F%91%E8%AF%AD%E8%A8%80/
)
5. [S1: Go](https://blog.csdn.net/stpeace/category_7234293.html)
6. [Go语言中文网](https://studygolang.com/resources/cat/1)
7. [RPCX](https://rpcx.io/)
8. [Golang 环境配置与应用编译](https://mp.weixin.qq.com/s?__biz=MzU3MjQ1ODcwNQ==&mid=2247483897&idx=1&sn=98dac43a26fd5d3be21077c4b203a3e8&chksm=fcd1d247cba65b5196f4ee3da0e43845e78961775c3e0bfc9a7756ecdae976b431cc2e3566b2&scene=21#wechat_redirect)
9. [//Go](https://blog.csdn.net/u014597198/category_8088292.html)  13篇

#### Freamwork Reference
1. [Golang 微服务框架 Go kit](https://blog.csdn.net/warrior_0319/article/details/77548958)
2. [Golang实现eureka-client](https://www.cnblogs.com/bener/p/10683404.html)
3. [Golang websocket推送](https://www.cnblogs.com/bener/p/10717466.html)
4. [Golang实现requests库](https://www.cnblogs.com/bener/p/10688294.html)
5. [Golang实现eureka-client](https://www.cnblogs.com/bener/p/10683404.html)

#### Utils
1. [gulu](https://hacpai.com/tag/gulu)
2. [xuanbo](https://github.com/xuanbo?tab=repositories)

### Local Env
#### Local Env Show Command
```bash
$ which go
/f/Go/bin/go

$ echo $GOPATH
F:\GoCenter

$ echo $GOROOT
F:\Go\
```

### Knowledge
```html
1, go get -d -v ./... #自动下载所有依赖包
Reference https://blog.csdn.net/weixin_30636089/article/details/99498728
eg:
F:\GoCenter\src\omc_engine>go get -d -v ./...
github.com/buaazp/fasthttprouter (download)
github.com/valyala/fasthttp (download)
github.com/klauspost/compress (download)
github.com/valyala/bytebufferpool (download)
github.com/prometheus/client_golang (download)
github.com/beorn7/perks (download)
github.com/cespare/xxhash (download)
package github.com/cespare/xxhash/v2: cannot find package "github.com/cespare/xxhash/v2" in any of:
        F:\Go\src\github.com\cespare\xxhash\v2 (from $GOROOT)
        F:\GoCenter\src\omc_engine\src\github.com\cespare\xxhash\v2 (from $GOPATH)
        F:\GoCenter\src\github.com\cespare\xxhash\v2
github.com/prometheus/client_model (download)
github.com/prometheus/common (download)
github.com/matttproud/golang_protobuf_extensions (download)
Fetching https://golang.org/x/sys/windows?go-get=1
Parsing meta tags from https://golang.org/x/sys/windows?go-get=1 (status code 200)
get "golang.org/x/sys/windows": found meta tag get.metaImport{Prefix:"golang.org/x/sys", VCS:"git", RepoRoot:"https://go.googlesource.com/sys"} at h
ttps://golang.org/x/sys/windows?go-get=1
get "golang.org/x/sys/windows": verifying non-authoritative meta tag
Fetching https://golang.org/x/sys?go-get=1
Parsing meta tags from https://golang.org/x/sys?go-get=1 (status code 200)
golang.org/x/sys (download)
github.com/oschwald/geoip2-golang (download)
github.com/oschwald/maxminddb-golang (download)
github.com/kylelemons/go-gypsy (download)
github.com/robfig/cron (download)
github.com/go-sql-driver/mysql (download)
Fetching https://golang.org/x/net/context?go-get=1
Parsing meta tags from https://golang.org/x/net/context?go-get=1 (status code 200)
get "golang.org/x/net/context": found meta tag get.metaImport{Prefix:"golang.org/x/net", VCS:"git", RepoRoot:"https://go.googlesource.com/net"} at h
ttps://golang.org/x/net/context?go-get=1
get "golang.org/x/net/context": verifying non-authoritative meta tag
Fetching https://golang.org/x/net?go-get=1
Parsing meta tags from https://golang.org/x/net?go-get=1 (status code 200)
golang.org/x/net (download)
Fetching https://google.golang.org/grpc?go-get=1
Parsing meta tags from https://google.golang.org/grpc?go-get=1 (status code 200)
get "google.golang.org/grpc": found meta tag get.metaImport{Prefix:"google.golang.org/grpc", VCS:"git", RepoRoot:"https://github.com/grpc/grpc-go"}
at https://google.golang.org/grpc?go-get=1
google.golang.org/grpc (download)
Fetching https://google.golang.org/grpc/metadata?go-get=1
Parsing meta tags from https://google.golang.org/grpc/metadata?go-get=1 (status code 200)
get "google.golang.org/grpc/metadata": found meta tag get.metaImport{Prefix:"google.golang.org/grpc", VCS:"git", RepoRoot:"https://github.com/grpc/g
rpc-go"} at https://google.golang.org/grpc/metadata?go-get=1
get "google.golang.org/grpc/metadata": verifying non-authoritative meta tag
Fetching https://google.golang.org/grpc?go-get=1
Parsing meta tags from https://google.golang.org/grpc?go-get=1 (status code 200)
Fetching https://google.golang.org/grpc/codes?go-get=1
Parsing meta tags from https://google.golang.org/grpc/codes?go-get=1 (status code 200)
get "google.golang.org/grpc/codes": found meta tag get.metaImport{Prefix:"google.golang.org/grpc", VCS:"git", RepoRoot:"https://github.com/grpc/grpc
-go"} at https://google.golang.org/grpc/codes?go-get=1
get "google.golang.org/grpc/codes": verifying non-authoritative meta tag
Fetching https://google.golang.org/grpc/status?go-get=1
Parsing meta tags from https://google.golang.org/grpc/status?go-get=1 (status code 200)
get "google.golang.org/grpc/status": found meta tag get.metaImport{Prefix:"google.golang.org/grpc", VCS:"git", RepoRoot:"https://github.com/grpc/grp
c-go"} at https://google.golang.org/grpc/status?go-get=1
get "google.golang.org/grpc/status": verifying non-authoritative meta tag
Fetching https://google.golang.org/genproto/googleapis/rpc/status?go-get=1
Parsing meta tags from https://google.golang.org/genproto/googleapis/rpc/status?go-get=1 (status code 200)
get "google.golang.org/genproto/googleapis/rpc/status": found meta tag get.metaImport{Prefix:"google.golang.org/genproto", VCS:"git", RepoRoot:"http
s://github.com/google/go-genproto"} at https://google.golang.org/genproto/googleapis/rpc/status?go-get=1
get "google.golang.org/genproto/googleapis/rpc/status": verifying non-authoritative meta tag
Fetching https://google.golang.org/genproto?go-get=1
Parsing meta tags from https://google.golang.org/genproto?go-get=1 (status code 200)
google.golang.org/genproto (download)
Fetching https://golang.org/x/text/secure/bidirule?go-get=1
Parsing meta tags from https://golang.org/x/text/secure/bidirule?go-get=1 (status code 200)
get "golang.org/x/text/secure/bidirule": found meta tag get.metaImport{Prefix:"golang.org/x/text", VCS:"git", RepoRoot:"https://go.googlesource.com/
text"} at https://golang.org/x/text/secure/bidirule?go-get=1
get "golang.org/x/text/secure/bidirule": verifying non-authoritative meta tag
Fetching https://golang.org/x/text?go-get=1
Parsing meta tags from https://golang.org/x/text?go-get=1 (status code 200)
golang.org/x/text (download)
Fetching https://golang.org/x/text/unicode/norm?go-get=1
Parsing meta tags from https://golang.org/x/text/unicode/norm?go-get=1 (status code 200)
get "golang.org/x/text/unicode/norm": found meta tag get.metaImport{Prefix:"golang.org/x/text", VCS:"git", RepoRoot:"https://go.googlesource.com/tex
t"} at https://golang.org/x/text/unicode/norm?go-get=1
get "golang.org/x/text/unicode/norm": verifying non-authoritative meta tag
github.com/aws/aws-sdk-go (download)
github.com/jolestar/go-commons-pool (download)
package github.com/jolestar/go-commons-pool/v2/collections: cannot find package "github.com/jolestar/go-commons-pool/v2/collections" in any of:
        F:\Go\src\github.com\jolestar\go-commons-pool\v2\collections (from $GOROOT)
        F:\GoCenter\src\omc_engine\src\github.com\jolestar\go-commons-pool\v2\collections (from $GOPATH)
        F:\GoCenter\src\github.com\jolestar\go-commons-pool\v2\collections
package github.com/jolestar/go-commons-pool/v2/concurrent: cannot find package "github.com/jolestar/go-commons-pool/v2/concurrent" in any of:
        F:\Go\src\github.com\jolestar\go-commons-pool\v2\concurrent (from $GOROOT)
        F:\GoCenter\src\omc_engine\src\github.com\jolestar\go-commons-pool\v2\concurrent (from $GOPATH)
        F:\GoCenter\src\github.com\jolestar\go-commons-pool\v2\concurrent


```

