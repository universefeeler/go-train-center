package main

// https://blog.csdn.net/warrior_0319/article/details/85045775
import (
log "github.com/sirupsen/logrus"
)

func main() {
	log.WithFields(log.Fields{
		"animal": "walrus",
	}).Info("A walrus appears")
}
