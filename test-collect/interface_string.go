package main


/**
https://www.cnblogs.com/sevenyuan/p/6382966.html
15、关于给类型添加String()方法   相当于 其他语言的toString 用于打印输出
 */
import "fmt"
//非侵入式接口
//接口 和实现完全分析 减少耦合
//实现方只负责实现  接口方只负责封装自己的借口就行...实现方甚至不知道 有这个接口的存在 这就是 Go的 非侵入式接口的特点
//16、接口的组合 就是把多个接口组合到一起......接口中只有函数没有属性
type IFly interface{
	fly()
}
type ISay interface{
	say()
}
type IFly1 interface{
	fly()
}
type ISay1 interface{
	say()
}
type Bird struct{

}
//由于匿名传递进来的是指针类型 所对于接口的赋值必须是 指针
func (pBird *Bird) fly(){
	fmt.Println("i am a bird, i can fly()!")
}
//由于匿名传递的不是指针类型是值类型 所以接口赋值 可以不是指针而是值
func (pBird Bird) say(){
	fmt.Println("i am a bird, i can say()!")
}
func (pBird Bird) String() string{
	return "aaaaaaaaaa"
}


func main(){
	birdObj:=Bird{}

	var iFly IFly=&birdObj
	iFly.fly()
	var iSay ISay=birdObj
	iSay.say()
	////接口之间的赋值
	var iFly1 IFly1=iFly
	iFly1.fly()
	fmt.Println(birdObj)
}


