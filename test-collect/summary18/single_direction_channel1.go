package main

import "fmt"
import "time"

// https://www.cnblogs.com/sevenyuan/p/6382932.html
// 12、只读只写 单向 channel 代码例子    遵循权限最小化的原则
//接受一个参数 是只允许读取通道  除非直接强制转换 要么你只能从channel中读取数据
func sCh(ch <-chan int){
	for val:= range ch {
		fmt.Println(val)
	}
}
func main(){
	//创建一个带100缓冲的通道 可以直接写入 而不会导致 主线程堵塞
	dch:=make(chan int,100)
	for i:=0;i<100;i++{
		dch<- i
	}
	//传递进去 只读通道
	go sCh(dch)
	time.Sleep(1e9)
}

/*13、channel的关闭,以及判断channel的关闭
关闭channel非常简单，直接使用Go语言内置的close()函数即可：
close(ch)
在介绍了如何关闭channel之后，我们就多了一个问题：如何判断一个channel是否已经被关
闭？我们可以在读取的时候使用多重返回值的方式：
x, ok := <-ch
这个用法与map中的按键获取value的过程比较类似，只需要看第二个bool返回值即可，如
果返回值是false则表示ch已经被关闭。*/


