package main


// https://www.cnblogs.com/sevenyuan/p/6382932.html
// Golang :不要通过共享内存来通信，而应该通过通信来共享内存。这句风靡在Go社区的话,说的就是 goroutine中的 channel .......
//他在go并发编程中充当着 类型安全的管道作用。
// 通过golang中的 goroutine 与sync.Mutex进行 并发同步
import(
	"fmt"
	"sync"
	"runtime"
)
var count int =0

func counter(lock * sync.Mutex){
	lock.Lock()
	count++
	fmt.Println(count)
	lock.Unlock()
}

func main(){
	lock:=&sync.Mutex{}
	for i:=0;i<10;i++{
		//传递指针是为了防止 函数内的锁和 调用锁不一致
		go counter(lock)
	}
	for{
		lock.Lock()
		c:=count
		lock.Unlock()
		///把时间片给别的goroutine  未来某个时刻运行该routine
		runtime.Gosched()
		if c>=10{
			fmt.Println("goroutine end")
			break
		}
	}
}