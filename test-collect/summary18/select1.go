package main

import (
	"time"
	"fmt"
)

// https://www.cnblogs.com/sevenyuan/p/6382932.html
// select是语言级内置  非堵塞
// https://www.jianshu.com/p/2a1146dc42c3
//;;;;;;;;案例1 如果有一个或多个IO操作可以完成，则Go运行时系统会随机的选择一个执行，否则的话，如果有default分支，则执行default分支语句，如果连default都没有，则select语句会一直阻塞，直到至少有一个IO操作可以进行
func main() {
	start := time.Now()
	c := make(chan interface{})
	ch1 := make(chan int)
	ch2 := make(chan int)

	go func() {
		time.Sleep(4*time.Second)
		close(c)
	}()

	go func() {
		time.Sleep(3*time.Second)
		ch1 <- 3
	}()

	go func() {
		time.Sleep(3*time.Second)
		ch2 <- 5
	}()

	fmt.Println("Blocking on read...")

	select {

	case <- c:
		fmt.Printf("Unblocked %v later.\n", time.Since(start))

	case <- ch1:
		fmt.Printf("ch1 case...")

	case <- ch2:
		fmt.Printf("ch1 case...")

	default:
		fmt.Printf("default go...")
	}
}
