package main

// https://www.cnblogs.com/sevenyuan/p/6382932.html
// 5、用goroutine模拟生产消费者
import "fmt"
import "time"


func Producer (queue chan<- int){
	for i:= 0; i < 10; i++ {
		fmt.Println("send:", i)
		queue <- i
	}
}
func Consumer( queue <-chan int){
	for i :=0; i < 10; i++{
		v := <- queue
		fmt.Println("receive:", v)
	}
}
func main(){
	queue := make(chan int, 1)
	go Producer(queue)
	go Consumer(queue)
	time.Sleep(1e9) //让Producer与Consumer完成
}

/*6、 通过make 创建通道
make(c1 chan int)   创建的是 同步channel ...读写完全对应
make(c1 chan int ,10) 闯进带缓冲的通道 上来可以写10次*/
