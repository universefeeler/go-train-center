package main

import (
	"fmt"
	"time"
)

// https://blog.csdn.net/qq_36431213/article/details/83281250

/*在Go中我们make一个channel有两种方式，分别是有缓冲的和没缓冲的
缓冲channel 即 buffer channel 创建方式为 make(chan TYPE,SIZE)
如 make(chan int,3) 就是创建一个int类型，缓冲大小为3的 channel
非缓冲channel 即 unbuffer channel 创建方式为 make(chan TYPE)
如 make(chan int) 就是创建一个int类型的非缓冲channel*/

func loop(ch chan int) {
	for {
		select {
		case i := <-ch:
			fmt.Println("this  value of unbuffer channel", i)
		}
	}
}

func main() {
	ch := make(chan int,3)
	ch <- 1
	ch <- 2
	ch <- 3
	ch <- 4
	go loop(ch)
	time.Sleep(1 * time.Millisecond)
}

/*之前我们示范创建的都是不带缓冲的channel，这种做法对于传递单个数据的场景可以接受，
但对于需要持续传输大量数据的场景就有些不合适了。接下来我们介绍如何给channel带上缓冲，
从而达到消息队列的效果。
要创建一个带缓冲的channel，其实也非常容易：
c := make(chan int, 1024)
在调用make()时将缓冲区大小作为第二个参数传入即可，比如上面这个例子就创建了一个大小
为1024的int类型channel，即使没有读取方，写入方也可以一直往channel里写入，在缓冲区被
填完之前都不会阻塞。
从带缓冲的channel中读取数据可以使用与常规非缓冲channel完全一致的方法，但我们也可
以使用range关键来实现更为简便的循环读取：
for i := range c {
fmt.Println("Received:", i)
} */



