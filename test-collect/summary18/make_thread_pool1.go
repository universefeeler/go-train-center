package main

// 9、关于创建多个goroutine具体到go语言会创建多少个线程
import "os"
func main() {
	for i:=0; i<20; i++ {
		go func() {
			for {
				b:=make([]byte, 10)
				os.Stdin.Read(b) // will block
			}
		}()
	}
	select{}
}

//会产生21个线程：
//runtime scheduler(src/pkg/runtime/proc.c)会维护一个线程池，当某个goroutine被block后，
//scheduler会创建一个新线程给其他ready的goroutine GOMAXPROCS控制的是未被阻塞的所有goroutine被multiplex到多少个线程上运行
