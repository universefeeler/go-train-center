package main

import (
	"fmt"
	"time"
)

/*非缓冲channel 和 缓冲channel 的区别
非缓冲 channel，channel 发送和接收动作是同时发生的
例如 ch := make(chan int) ，如果没 goroutine 读取接收者<-ch ，那么发送者ch<- 就会一直阻塞
缓冲 channel 类似一个队列，只有队列满了才可能发送阻塞*/


// https://blog.csdn.net/qq_36431213/article/details/83281250
func loop(ch chan int) {
	for {
		select {
		case i := <-ch:
			fmt.Println("this  value of unbuffer channel", i)
		}
	}
}

func main() {
	ch := make(chan int)
	ch <- 1
	go loop(ch)
	time.Sleep(1 * time.Millisecond)
}



