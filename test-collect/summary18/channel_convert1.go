package main

// 我们默认创建的是双向通道,单向通道没有意义,但是我们却可以通过强制转换 将双向通道 转换成为单向通道 。

// channel是一个原生类型，因此不仅 支持被传递，还支持类型转换。只有在介绍了单向channel的概念后，读者才会明白类型转换对于
// channel的意义：就是在单向channel和双向channel之间进行转换。


func main()  {

	var ch1 chan int  // ch1是一个正常的channel，不是单向的
	var ch2 chan<- float64// ch2是单向channel，只用于写float64数据
	var ch3 <-chan int // ch3是单向channel，只用于读取int数据

	ch4 := make(chan int)
	ch5 := <-chan int(ch4) // ch5就是一个单向的读取channel
	ch6 := chan<- int(ch4) // ch6 是一个单向的写入channel

}

// 基于ch4，我们通过类型转换初始化了两个单向channel：单向读的ch5和单向写的ch6。
// 从设计的角度考虑，所有的代码应该都遵循“最小权限原则” ，
// 从而避免没必要地使用泛滥问题， 进而导致程序失控。 写过C++程序的读者肯定就会联想起const 指针的用法。非const指针具备const指针的所有功能，将一个指针设定为const就是明确告诉
// 函数实现者不要试图对该指针进行修改。单向channel也是起到这样的一种契约作用。
