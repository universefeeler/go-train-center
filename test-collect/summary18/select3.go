package main

import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382932.html
/*可以看出，select不像switch，后面并不带判断条件，而是直接去查看case语句。每个
case语句都必须是一个面向channel的操作。比如上面的例子中，第一个case试图从chan1读取
一个数据并直接忽略读到的数据，而第二个case则是试图向chan2中写入一个整型数1，如果这
两者都没有成功，则到达default语句。*/
func main() {
	ch1 := make(chan int, 1)
	ch2 := make(chan int, 1)
	ch1 <- 3
	ch2 <- 5
	select {
	case <- ch1:
		fmt.Println("ch1 selected.")
		break
		fmt.Println("ch1 selected after break")
	case <- ch2:
		fmt.Println("ch2 selected.")
		fmt.Println("ch2 selected without break")
	}
}

/*select {
case <-chan1: // 如果chan1成功读到数据，则进行该case处理语句
case chan2 <- 1: // 如果成功向chan2写入数据，则进行该case处理语句
default: // 如果上面都没有成功，则进入default处理流程
}*/

