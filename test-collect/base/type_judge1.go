package main

import(
"fmt"
)

// https://blog.csdn.net/vigiking/article/details/90443075
// 类型断言用于提取接口的基础值，语法：i.(T)
func assert(i interface{}){
	s:= i.(int)
	fmt.Println(s)
}

func main(){
	var s interface{} = 55
	assert(s)
}