package main

import "fmt"


// https://blog.csdn.net/K346K346/article/details/88798614
var m = map[string]*struct{x, y int} {
	"foo": {2, 3},
}
func main() {
	m["foo"].x = 4
	fmt.Println(m["foo"].x)
}

