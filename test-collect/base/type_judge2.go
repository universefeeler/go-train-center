package main


import (
"fmt"
)

// https://blog.csdn.net/vigiking/article/details/90443075
func assert(i interface{}) {
	v, ok := i.(int)
	fmt.Println(v, ok)
}

func main() {
	var s interface{} = 56
	assert(s)
	var i interface{} = "Steven Paul"
	assert(i)
}