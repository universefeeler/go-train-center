package main

//https://blog.csdn.net/K346K346/article/details/88798614
func main() {

	//var 申明
	var i int

	//var 申明指定值
	var i int = 1

	//var 申明省略变量类型
	var i = 1

	//var 一行申明多个
	var i, j int

	//短声明格式，不能指明类型且只限于函数内
	i := 1

	//短声明格式，一行申明多个，只限于函数内
	i, j := 1, 2


}
