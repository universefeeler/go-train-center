package main

import "fmt"
// https://blog.csdn.net/K346K346/article/details/88798614

var m = map[string]struct{x, y int} {
	"foo":{2, 3},
}

func main() {
	tmp:=m["foo"]
	tmp.x=4
	m["foo"]=tmp
	fmt.Println(m["foo"].x)
}

