package main

import (
	"fmt"
)

func add(x , y int) int {
	return x + y
}

func main() {
	fmt.Println("main")
	i := 3 ; j := 2
	fmt.Println(i, j)
	sum := add(i, j)
	fmt.Println(sum)
}