package main


import (
"fmt"
)

type Student struct {
	Sno             int     		//整型（数值类型）
	Name            string  		//字符串
	Gender          bool    		//布尔类型
	Age             float32 		//浮点型（数值类型）
}

type StudentPrint interface {
	Print()
}

func main(){
	stu := new(Student)
	fmt.Printf("stu=%+v\n", stu)

	var sliceStudent        []Student			//切片
	var mapStudent          map[int]Student		//映射
	var pStu                *Student			//指针
	fmt.Printf("slice=%v map=%v pointer=%v\n", sliceStudent, mapStudent, pStu)

	var ifVar       StudentPrint				//接口
	var funcVar     func(str *Student)			//函数
	var chVar       chan Student 				//信道
	fmt.Printf("interface=%v func=%v chan=%v\n", ifVar, funcVar, chVar)
}

