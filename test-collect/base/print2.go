package main

import (
"fmt"
)

func main(){
	{
		x := 123
		fmt.Printf("type is %T\n", x)
	}
	{
		x := "123"
		fmt.Printf("type is %T\n", x)
	}
}
