package main

import "fmt"

// https://blog.csdn.net/vigiking/article/details/90377597
// https://blog.csdn.net/lbPro0412/article/details/81303378
// 闭包是由函数及其相关引用环境组合而成的实体(即：闭包=函数+引用环境)。
func fib() func() int32 {
	var a, b, c int32 = 1, 1, 1
	return func() (res int32) {

		if c <= 2 {
			c++
			res = 1

		} else {
			b, a = a, a+b
			res = a
		}

		return
	}
}

func main() {
	var x = fib()
	for i := 1; i < 10; i++ {
		fmt.Println(x())
	}

}