package main

import "fmt"

//https://blog.csdn.net/lwldcr/article/details/77370948
//interface{}意味着您可以放置任何类型的值，包括您自己的自定义类型。Go中的所有类型都满足空接口（interface{}是一个空接口）。


type Person interface {
	SayHi() interface{} // 如果此方法返回值换成[]interface{} 会报错
}

type Man struct {
	Name string
}

func (m Man) SayHi() interface{} {
	return []byte("Hi, my name is: " + m.Name)
}

type Women struct {
	Name string
}

func (w Women) SayHi() interface{} {
	return []byte("Hi, my name is: " + w.Name)
}

func sayHi(p Person) {
	fmt.Println(string(p.SayHi().([]byte))) // 转换为[]byte类型
}

func main() {
	m := Man{Name: "Bruce"}
	w := Women{
		Name: "Anna",
	}

	sayHi(m)
	sayHi(w)
}