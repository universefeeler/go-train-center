package main

import "fmt"

/**
https://www.cnblogs.com/sevenyuan/p/6382966.html
17、接口查询 obj,ok=val.(Interface)   返回查询的接口 并且返回查询结果
x.(type) 获取类型 只能在switch中用
x.(OterTypeInterface) 判断x是否是指定接口类型 返回指定接口对象,和查询结果
在Go语言中，还可以更加直截了当地询问接口指向的对象实例的类型，例如：

当然，Go语言标准库的Println()比这个例子要复杂很多，我们这里只摘取其中的关键部分进行分析。对于内置类型，Println()采用穷举法，将每个类型转换为字符串进行打印。
对于更一般的情况，首先确定该类型是否实现了String()方法，如果实现了，则用String()方法将其转换为字符串进行打印。
否则，Println()利用反射功能来遍历对象的所有成员变量进行打印。是的，利用反射也可以进行类型查询，详情可参阅reflect.TypeOf()方法的相关文档。此外，
 */
func main() {

	var v1 interface{} = 1
	switch v := v1.(type) {
	case int:    // 现在v的类型是int
		fmt.Println(v)
	case string: // 现在v的类型是string
		fmt.Println(v)
	}


}
