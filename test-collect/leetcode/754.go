package main

import "fmt"

// https://blog.csdn.net/vigiking/article/details/90726460


func reachNumber(target int) int {
	sum, i:=0, 1
	if target < 0{
		target = -target
	}
	for ; sum < target || (sum-target)%2 != 0;i++{
		sum += i
	}
	return  i-1
}

func main() {

	t := reachNumber(3)
	fmt.Printf("%d", t)

}
