package main

import "fmt"

// https://blog.csdn.net/vigiking/article/details/90441371
func trailingZeroes(n int) int {
	var count int
	if n == 0 {
		count =  0
	}else{
		count = n / 5 + trailingZeroes(n / 5)
	}
	return count
}

func main() {

	fmt.Println(trailingZeroes(10))

}
