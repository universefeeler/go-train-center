package main

import "fmt"


func isRobotBounded(instructions string) bool {
	x, y,direction,count := 0, 0, 0,0
	for{
		for _,v:=range instructions{
			switch v{
			case 'G':{
				switch direction{
				case 0:{
					y++
				}
				case 1,-3:{
					x++
				}
				case 2,-2:{
					y--
				}
				case 3,-1:{
					x--
				}

				}
			}
			case 'L':{
				direction--
				direction %= 4

			}
			case 'R':{
				direction++
				direction %= 4
			}
			}
		}
		count++
		if x == 0 && y == 0{
			return true
		}
		if count>=4{
			break
		}
	}

	return false
}

func main() {

	t := isRobotBounded("GG")
	fmt.Printf("%s", t)

}
