package main

import "fmt"
// https://www.cnblogs.com/sevenyuan/p/6382966.html
// 18、 Any类型  对于匿名结构体赋值给任意类型  没法取出 具体每个匿名结构体的内部属性 只能前部打印 通过系统默认的String()函数
func main() {
	var any1 interface{}=1
	var any2 interface{}="b"
	var any3 interface{}=struct{x ,y string}{"hello,world","aaaaa"}
	fmt.Println(any1,any2,any3)
    // 由于Go语言中任何对象实例都满足空接口interface{}，所以interface{}看起来像是可以指向任何对象的Any类型，如下：
	var v1 interface{} = 1       // 将int类型赋值给interface{}
	var v2 interface{} = "abc"   // 将string类型赋值给interface{}
	var v3 interface{} = &v2     // 将*interface{}类型赋值给interface{}
	var v4 interface{} = struct{ X int }{1}
	var v5 interface{} = &struct{ X int }{1}
	fmt.Println(v1,v2,v3,v4,v5)
	// 当函数可以接受任意的对象实例时，我们会将其声明为interface{}，最典型的例子是标准库fmt中PrintXXX系列的函数，例如：
	//func Printf(fmt string, args ...interface{})
	//func Println(args ...interface{})

	//总体来说，interface{}类似于COM中的IUnknown，我们刚开始对其一无所知，但可以通过接口查询和类型查询逐步了解它。
	//由于Go语言中任何对象实例都满足空接口interface{}，所以interface{}看起来像是可以指向任何对象的Any类型，如下：
	// 总体来说，interface{}类似于COM中的IUnknown，我们刚开始对其一无所知，但可以通过接口查询和类型查询逐步了解它。
}