package main

// https://mp.weixin.qq.com/s/bWsg-ZDBTp3Fp_3Lik83oA

type Talkable interface {
	TalkEnglish(string)
	TalkChinese(string)
}

type Student1 struct {
	Talkable
	Name string
	Age  int
}

// test 1
/*func main(){
    // 一种取巧的方法，将 interface 嵌入结构体，可以使该类型快速实现该 interface。
	a := Student1{Name: "aaa", Age: 12}
	var b Talkable = a
	fmt.Println(b)
}*/

// test 2
func main(){
	// 并没有实现 interface 的方法，当然会报错。我们可以只实现 interface 的一部分方法，比如我只需要用到 Talkable 的 TalkEnglish 方法
	a := Student1{Name: "aaa", Age: 12}
	a.TalkEnglish("nice to meet you\n")
}