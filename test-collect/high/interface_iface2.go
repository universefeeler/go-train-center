package main

import "fmt"

// https://mp.weixin.qq.com/s/bWsg-ZDBTp3Fp_3Lik83oA

/**
type iface struct {
	tab  *itab
	data unsafe.Pointer
}

type eface struct {
	_type *_type
	data  unsafe.Pointer
}
 */

type Student struct {
	Name string
}


 func main() {
	 a := Student{Name:"aaa"}
	 var b interface{} = a
	 a.Name = "bbb"
	 fmt.Println(b.(Student).Name)

 }
