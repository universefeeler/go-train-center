package main

import (
	"time"
	"fmt"
)

// https://mp.weixin.qq.com/s/FUI-Eoc6V2e5RhDSFxy6Dw
// 事实上使用 chan 的好处不仅在于解决了并发访问的 data race 和锁的问题，
// 而且还提高了代码的运行效率——chan 的接收端 goroutine 会被挂起 直到 chan 中有值可读，
// 相比于传统方法的循环检测共享变量，这种方式效率明显要高不少，且优雅。
func Setup2() <-chan bool {
	time.Sleep(time.Second * 3)
	c := make(chan bool)
	c <- true
	return c
}

func main() {
	if <-Setup2(){
		fmt.Println("setup succeed")
	}
}
