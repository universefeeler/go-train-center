package main

// https://blog.csdn.net/warrior_0319/article/details/79008803
import (
	"fmt"
	"sync"
	"net/http"
)

func main() {
	var wg sync.WaitGroup
	var urls = []string{
		"http://www.golang.org/",
		"http://www.google.com/",
		"http://www.baiyuxiong.com/",
	}
	for _, url := range urls {
		// WaitGroup增加一个计数.
		wg.Add(1)
		// 执行goroutine抓取网页.
		go func(url string) {
			// 当goroutine完成是，减少WaitGroup的计数.
			defer wg.Done()
			// Fetch the URL.
			http.Get(url)
			fmt.Println(url);
		}(url)
	}
	// 等待所有的 HTTP 完成.
	wg.Wait()
	fmt.Println("over");
}
