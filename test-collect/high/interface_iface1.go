package main

import "fmt"

// https://mp.weixin.qq.com/s/bWsg-ZDBTp3Fp_3Lik83oA

/**
type iface struct {
	tab  *itab
	data unsafe.Pointer
}

type eface struct {
	_type *_type
	data  unsafe.Pointer
}
 */

type Student struct {
	Name string
}
var b interface{} = Student{
	Name:     "aaa",
}

 func main() {
	 var c  = b.(Student)
	 c.Name = "bbb" //实际的过程是首先将 b 指向的数据复制一份，然后转换为 Student 类型赋值给 c。
	 fmt.Println(b.(Student).Name)
 }