package main

import (
	"io"
	"fmt"
)

// https://mp.weixin.qq.com/s/bWsg-ZDBTp3Fp_3Lik83oA

/**
type iface struct {
	tab  *itab
	data unsafe.Pointer
}

type eface struct {
	_type *_type
	data  unsafe.Pointer
}
 */

 // 当一个指针赋值给 interface 类型时，无论此指针是否为 nil，赋值过的 interface 都不为 nil。

func GetReader(id int64) io.Reader {
	var r *MyReader = nil
	if id > 0 && id < 10000{
		r = openReader(id)
	}
	return r
}
func main() {
	r := GetReader(-2)
	if r == nil {
		fmt.Println("bad reader")
	} else {
		fmt.Println("valid reader")
	}
}
