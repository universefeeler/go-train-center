package main

import (
	"sync"
	"time"
	"fmt"
)

var inited = false
var lock sync.Mutex
// https://mp.weixin.qq.com/s/FUI-Eoc6V2e5RhDSFxy6Dw
// 如何解决上边的 data race 问题。实际上在 golang 中，互斥锁在并发操作中是非常常见的，当某个变量存在并发访问的可能时，请一定记得加锁
func Setup(){
	time.Sleep(time.Second)
	lock.Lock()
	inited = true
	lock.Unlock()
}

func main() {
	go Setup()

	for{
		lock.Lock()
		b := inited
		lock.Unlock()
		if b{
			break
		}
		time.Sleep(100 * time.Millisecond)
	}

	fmt.Println("setup succeed")
}