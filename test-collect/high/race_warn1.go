package main

import (
	"time"
	"fmt"
)

// https://mp.weixin.qq.com/s/FUI-Eoc6V2e5RhDSFxy6Dw

// -race 用于检测代码中可能存在的 data race。这里的告警信息写得很清楚，一个 goroutine 的写操作和另一个 goroutine 的读操作可能发生 data race，有风险。
var inited = false

func Setup(){
	time.Sleep(time.Second)
	inited = true
}
func main() {
	go Setup()

	for{
		if inited{
			break
		}
		time.Sleep(100 * time.Millisecond)
	}

	fmt.Println("setup succeed")
}