package main

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// import strings 这个包中包含了处理string类型的所有工具函数函数 Go编译器支持UTF-8的源代码文件格式。
// 这意味着源代码中的字符串可以包含非ANSI的字符，比如“Hello world. 你好，世界！ ”可以出现在Go代码中。但需要注意的是，如果你的Go代码需要包含非ANSI字符，保存源文件时请注意编码格式必须选择UTF-8。特别是在Windows下一般编辑器都默认存为本地编码，比如中国地区可能是GBK编码而不是UTF-8，如果没注意这点在编译和运行时就会出现一些意料之外的情况。 字符串的编码转换是处理文本文档（比如TXT、XML、HTML等）非常常见的需求，不过可惜的是Go语言仅支持UTF-8和Unicode编码。对于其他编码，Go语言标准库并没有内置的编码转换支持。不过，所幸的是我们可以很容易基于iconv库用Cgo包装一个。
func main()  {
	// https://github.com/xushiwei/go-iconv
}
