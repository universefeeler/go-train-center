//recover场景2
package main
import "fmt"

// Go语言引入了两个内置函数panic()和recover()以报告和处理运行时错误和程序中的错误场景：
//func panic(interface{})
//func recover() interface{}
//当在一个函数执行过程中调用panic()函数时，正常的函数执行流程将立即终止，但函数中之前使用defer关键字延迟执行的语句将正常展开执行，之后该函数将返回到调用函数，并导致逐层向上执行panic流程，直至所属的goroutine中所有正在执行的函数被终止。错误信息将被报告，包括在调用panic()函数时传入的参数，这个过程称为错误处理流程。
//从panic()的参数类型interface{}我们可以得知，该函数接收任意类型的数据，比如整型、字符串、对象等。调用方法很简单，
// recover()函数用于终止错误处理流程。一般情况下，recover()应该在一个使用defer
//关键字的函数中执行以有效截取错误处理流程。如果没有在发生异常的goroutine中明确调用恢复过程（使用recover关键字） ，会导致该goroutine所属的进程打印异常信息后直接退出。

func  main() {
	defer func(){
		fmt.Println("hello,defer go")
	}()
	panic(11111)
	panic(404)
	panic("network broken")
}