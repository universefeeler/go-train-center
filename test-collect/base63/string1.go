package main

import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// 33 34 在Go中字符串的单个字符就是 byte类型也就是 uint8

//35、关于 Go语言中支持的两种字符类型 一种是 byte 实际上是uint8的别名 ,另一种是unicode类型的字符 关键字为 rune
//在Go语言中支持两个字符类型，一个是byte（实际上是uint8的别名） ，代表UTF-8字符串的单个字节的值；另一个是rune，代表单个Unicode字符。
//关于rune相关的操作，可查阅Go标准库的unicode包。另外unicode/utf8包也提供了 UTF8和Unicode之间的转换。
//出于简化语言的考虑，Go语言的多数API都假设字符串为UTF-8编码。尽管Unicode字符在标 准库中有支持，但实际上较少使用。
func main() {
	var str string
	str = "abcdefghijklmn"
	var length int8=int8(len(str))
	for i:=0 ;i<int(length) ;i++{
		fmt.Printf("%c-",str[i])
	}

	// 关于遍历Unicode字符,每个 unicode的字符类型是 rune
	// 每个中文字符在UTF-8中占3个字节，而不是1个字节。
	str1 := "Hello,世界"
	for i, ch := range str1 {
		fmt.Println(i, ch)//ch的类型为rune
	}

	var strUnicode string = "hello,世界"
	for i,ch := range strUnicode{
		fmt.Println(i,ch)
	}
}
