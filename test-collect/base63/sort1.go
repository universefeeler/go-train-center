package main
import "fmt"
import "math/rand"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
/////冒泡排序Go实现 时间复杂富 O(n)=n~n^2
func bubbledSort(values []int){
	var flags bool =true
	for i:=0 ;i<len(values);i++{
		flags=true
		for j:=0;j<len(values)-i-1;j++{
			if values[j]>values[j+1] {
				values[j],values[j+1]=values[j+1],values[j]
				flags=false
			}
		}
		if flags {
			break
		}
	}
}
///////快速排序Go实现
func quickSort(values []int,left,right int){
	temp := values[left]
	p := left
	i, j := left, right
	for i <= j {
		for j >= p && values[j] >= temp {
			j--
		}
		if j >= p {
			values[p] = values[j]
			p = j
		}
		if values[i] <= temp && i <= p {
			i++
		}
		if i <= p {
			values[p] = values[i]
			p = i
		}
	}
	values[p] = temp
	if p - left > 1 {
		quickSort(values, left, p - 1)
	}
	if right - p > 1 {
		quickSort(values, p + 1, right)
	}
}
func  main() {
	//创建初始化0个元素  容量1000的 切片 如果用索引直接访问切片会越界的  容量必须大于等于 初始化元素个数
	//val[1]=11
	val:=make([]int,0,1000)
	for i:=0;i<1000;i++{
		val=append(val,rand.Intn(1000))
	}
	fmt.Println("冒泡排序前:",val)
	bubbledSort(val)
	fmt.Println("冒泡排序后:",val)
}