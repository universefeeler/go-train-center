package main
import "fmt"
var str string="aaa"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// 61、关于const 和iota的使用
const(
	A=iota
	B
	C
)
func  main() {
	fmt.Println(B)
	defer func(){
		if r:=recover() ;r!=nil{
			fmt.Println("recover() called!")
			fmt.Println(r)
		}
		fmt.Println("hello,defer go")
	}()
	panic("hello,go")
}