package main

import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// 关于Go的数组 是一个值类型,在做为参数传递 或者 做为函数返回的时候 都是 数组的副本,所以不能通过传递 数组参数在函数内部 进行修改 。
func modify(arr[5]int){
	arr[1]=1
	fmt.Printf("arr[1]=%d\n",arr[1])
}

func main()  {
	td:=[5]int{1,2,3,4,5}
	modify(td)
	for _,val:=range td{
		fmt.Println(val)
	}
}
