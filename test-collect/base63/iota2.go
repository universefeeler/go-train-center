package main

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// 如果两个const的赋值语句的表达式是一样的，那么可以省略后一个赋值表达式。因此，上
//面的前两个const语句可简写为：
func main() {
	const (     // iota被重设为0
		c0 = iota   // c0 == 0
		c1   // c1 == 1
		c2   // c2 == 2
	)
	const (
		a = 1 <<iota     // a == 1 (iota在每个const开头被重设为0)
		b   // b == 2
		c   // c == 4
	)
}
