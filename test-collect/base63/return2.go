package main

import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// 55、函数多返回值
// 56、对于结构类型空的值是nil
// 63、Go中的import和package 等等的关系
//import只是引入的文件夹而已,,,可以使用相对路径或者绝对路径
//他会把文件夹中的所有.go文件引入 ,,,,,,
///package xx 实际上是 在外不用调用用的 比如xx.A() 并不是给import使用   package内部的小写全部是私有 大写全部是公有
//外部包不能和主模块放到一起会编译不过的
//import "./bubble"
func ret()(int,int){
	return 1,2
}

func main() {
	a,b:=ret()
	fmt.Println("a,b=",a,b)
}
