package main

import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
func main() {

	var i int = 8 //正确的使用方式1
	var j = 9 //正确的使用方式2，编译器可以自动推导出j的类型
	i, j = j, i //支持多重赋值,i, j = j, i  两个值可以如此简单的进行交换  而不许引入外部变量
	fmt.Println(i, j)

}
