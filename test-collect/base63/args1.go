package main


import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// 49、函数的不定参数  实际上是一种"语法糖"
func show(args ...int){
	for _,val:= range args{
		fmt.Println(val)
	}
}

/*func myfunc(args ...int) {
// 按原样传递
  myfunc3(args...)
// 传递片段，实际上任意的int slice都可以传进去
 myfunc3(args[1:]...)
}*/

func main() {
	show(1,2,3)
}