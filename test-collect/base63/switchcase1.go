package main

import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// switch case default用法
// Go语言的goto 和break Label更加的灵活处理 循环和跳转

/*type Info struct{ // 自定义复杂数据类型
	name string
	age  int8
}*/

func main() {
	i := 2
	switch i {
	case 0:
		fmt.Printf("0")
	case 1:
		fmt.Printf("1")
	case 2:
		fallthrough
	case 3:
		fmt.Printf("3")
	case 4, 5, 6:
		fmt.Printf("4, 5, 6")
	default:
		fmt.Printf("Default")
	}
}
