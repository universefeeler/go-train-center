package main

import "fmt"

func returnFunc(num int) int{
	if num > 0 {
		return 100  //错误 返回值不能写在if...else之中结构之中
	}
	return 100
}

func main() {
	fmt.Println(returnFunc(5))
}
