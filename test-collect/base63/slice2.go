package main
import "fmt"

// 主动创建数组切片 操作数组 所有的方法 都是适用于数组切片 ,合理利用切片能极大提高内存操作的速度
// 数组切片支持内建的cap()函数和len()函数
// 从数组切片创建数组切片的时候只要不超过模板切片的大小那么创建是没问题的,否则会报出数组切片越界的错误。
func mod(arr[]int){
	arr[1]=1
	fmt.Printf("arr[1]=%d\n",arr[1])
}
func main() {
	td:=[]int{1,2,3,4,5}  //这样创建出来的实际上是数组切片
	//基于数组创建切片
	slice:=td[:3]     //从切片创建切片 都可以
	mod(slice)
	for _,val:=range slice{
		fmt.Println(val)
	}
}