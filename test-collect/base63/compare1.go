package main

import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// 24、Go语言支持以下的几种比较运算符：>、<、==、>=、<=和!=。这一点与大多数其他语言相 同，与C语言完全一致。
// 25、两个不同类型的值不能比较 比如 int8 int16，只能强制转换 然后再做比较
// 26、虽然两个 int8 int16不能直接比较 但是 任何整数类型都能和字面常量整数进行比较  ,但是不能和字符串字面常量进行比较
func main() {
	i,j:=1,2
	if i==j{ //必须带大括号
		fmt.Println("i==j");
	} else {
		fmt.Println("i!=j");
	}
	var a int8
	var b int16
	a,b=1,2
	if int16(a)==b{
		fmt.Printf("a==b")
	}

	if a==1{
		fmt.Printf("a!=\"a\"");
	}

}
