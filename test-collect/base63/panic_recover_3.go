package main

import "log"


// recover()函数用于终止错误处理流程。一般情况下，recover()应该在一个使用defer
//关键字的函数中执行以有效截取错误处理流程。如果没有在发生异常的goroutine中明确调用恢复过程（使用recover关键字） ，会导致该goroutine所属的进程打印异常信息后直接退出。
//以下为一个常见的场景。

//无论foo()中是否触发了错误处理流程，该匿名defer函数都将在函数退出时得到执行。假如foo()中触发了错误处理流程，recover()函数执行将使得该错误处理过程终止。
// 如果错误处理流程被触发时，程序传给panic函数的参数不为nil，则该函数还会打印详细的错误信息。
func foo() {
}

func main() {

	defer func() {
		if r := recover(); r != nil {
			log.Printf("Runtime error caught: %v", r)
		}
	}()
	foo()

}
