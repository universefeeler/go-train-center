package main

import "fmt"

// 关于Go语言的指针操作
// https://www.cnblogs.com/sevenyuan/p/6382940.html
func main() {
	var inta int8=3
	var pinta*int8 = &inta
	fmt.Printf("%d",*pinta)
	fmt.Printf("%s",pinta)
}

