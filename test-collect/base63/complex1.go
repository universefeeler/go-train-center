package main

import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// Go语言中的复数类型 , real 取出 实部  imag取出虚部  虚部为0 的复数 为纯虚数。 某一类数字可以表示成这种复数类型
func main() {
	var v1 complex64
	v1 = 2.5+15i
	v2 := 2.5+15i
	v3 :=complex(2.5,15)
	fmt.Println(v1)
	fmt.Println(v2)
	fmt.Println(v3)
	fmt.Println("real:",real(v1))
	fmt.Println("real:",imag(v1))
}
