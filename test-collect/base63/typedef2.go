///////////////////给结构体起个别名
package main
import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
type Data1 struct{
	name string
	age  int
}

type DData Data1

func  main() {
	data:=DData{"a",1}
	fmt.Println(data)
}


