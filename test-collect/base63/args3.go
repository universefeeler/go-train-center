package main
import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// Go的不定参数语法糖会把 参数构成一个数组切片 当然你直接传递  切片数组是不可以的,因为他的参数就是1,2,3,4,5,66,7,  但是我们可以通过...的方式打乱 数组 或者数组切片
//...只可以打乱数组切片,常规数组是个值类型你是无法操作的
func show3(args ...int){
	for _,val:= range args{
		fmt.Println(val)
	}
}
// ...
func Printfx3(args ...interface{}) {
	for _,val:= range args{
		fmt.Println(val)
	}
}
func main() {
	Printfx3(1,2,3,"adsd","sdaddf")
	slice:=[]int{5,4,3,2,6,7,8}
	show3(slice...)
}