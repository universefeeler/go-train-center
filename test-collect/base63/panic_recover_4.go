package main

import "fmt"


// https://www.cnblogs.com/sevenyuan/p/6382940.html
// 在panic()开始错误处理流程,传入的类型是interface{}任意类型  ,  如果我们在defer 函数中调用
//recover()函数那么会打断错误处理流程。 panic的调用会终止正常的程序流程
//recover()函数用于终止错误处理流程。一般情况下，recover()应该在一个使用defer
//关键字的函数中执行以有效截取错误处理流程。如果没有在发生异常的goroutine中明确调用恢复
//过程（使用recover关键字） ，会导致该goroutine所属的进程打印异常信息后直接退出。

func  main() {
	//通过闭包定义 defer匿名函数 并且直接调用
	defer func(){
		//recover 结束当前错误处理过程  并且返回 panic的参数,并不结束其他goroutine的执行.......
		if r:=recover() ;r!=nil{
			fmt.Println("recover() called!")
		}
		fmt.Println("hello,defer go")
	}()
	panic("hello,go")
}