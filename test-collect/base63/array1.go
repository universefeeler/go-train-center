package main

import "fmt"
// https://www.cnblogs.com/sevenyuan/p/6382940.html
// 关于数组的遍历 range 遍历可以选择忽略 索引
func main() {
	byteArr:=[5]byte{1,2,3,4,5}
	for _,val:=range byteArr {
		fmt.Println(val)
	}
}
