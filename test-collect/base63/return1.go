package main
import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// 关于函数返回 一个值和函数返回多个值
// 48 Go牢记这样的规则：小写字母开头的函数只在本包内可见，大写字母开头的函数才能被其他包使用。
func ret1() int{
	return 1
}
func ret2()(a int,b int){
	a,b=1,2
	return
}

func ret3()(a int,b int){
	a,b=2,1
	return a, b
}

func main() {
	a,b:=ret2()
	fmt.Println(ret1(),a,b)
	fmt.Println(ret3())
}