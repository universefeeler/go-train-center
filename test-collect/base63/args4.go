package main


import "fmt"

// 53、通过传递不定参数获取  任意类型不定参数的类型
func checkType(args...interface{}){
	for _,val:=range args{
		switch val.(type){
		case int :
			fmt.Println("Type is int!")
		case string:
			fmt.Println("Type is string!")
		default:
			fmt.Println("Type is unknow!")
		}
	}
}

func  main() {
	checkType(1,2,3,"aaaa",int64(22))
}