package main

import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// Go语言中的数组切片   可以从一个已存在的数组创建 也可以直接手动创建一个数组切片
// 一个指向原生数组的指针；
// 数组切片中的元素个数；
// 数组切片已分配的存储空间。
//从底层实现的角度来看，数组切片实际上仍然使用数组来管理元素，因此它们之间的关系让
//C++程序员们很容易联想起STL中std::vector和数组的关系。基于数组，数组切片添加了一系
//列管理功能，可以随时动态扩充存放空间，并且可以被随意传递而不会导致所管理的元素被重复
// 基于数组创建切片
func main() {
	td:=[]int{1,2,3,4,5}
	//基于数组创建切片
	slice:=td[:3]    //  td[:]  td[begin:end] 都可以创建数组切片  还可以创建一个比数组还大的切片
	for _,val:=range slice{
		fmt.Println(val)
	}
	for k,val:=range slice{
		fmt.Println(k, val)
	}
}
