package main

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// Go语言预定义了这些常量：true、false和iota。
// iota比较特殊，可以被认为是一个可被编译器修改的常量，在每一个const关键字出现时被
// 重置为0，然后在下一个const出现之前，每出现一次iota，其所代表的数字会自动增1。
func mian() {
	const (     // iota被重设为0
		c0 = iota  // c0 == 0
		c1 = iota  // c1 == 1
		c2 = iota  // c2 == 2
	)
	const (
		a = 1 << iota // a == 1 (iota在每个const开头被重设为0)
		b = 1 << iota  // b == 2
		c = 1 << iota  // c == 4
	)
	const (
		u = iota * 42    // u == 0
		v float64 = iota * 42   // v == 42.0
		w  = iota * 42   // w == 84
	)
	const x = iota      // x == 0 (因为iota又被重设为0了)
	const y = iota      // y == 0 (同上)

}


