package main

import (
	"fmt"
)
// https://www.cnblogs.com/sevenyuan/p/6382940.html
//函数的闭包定义 直接调用 .......闭包内部使用的代码块外部的变量 只要代码块没有释放那么变量不会被释放的
func main() {
	var j int = 5
	a := func()(func()) {
		var i int = 10
		return func() {
			fmt.Printf("i, j: %d, %d\n", i, j)
		}
	}()
	a()
	j *= 2
	a()
}
