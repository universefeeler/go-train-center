package main
import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// ...   传递任意类型的参数
func Printfx(args ...interface{}) {
	for _,val:= range args{
		fmt.Println(val)
	}
}
func main() {
	Printfx(1,2,3,"adsd","sdaddf")
}