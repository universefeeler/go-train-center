package main

import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html

/*出现在:=左侧的变量不应该是已经被声明过的，否则会导致编译错误，比如下面这个
写法：
var i int
i := 2
会导致类似如下的编译错误：
no new variables on left side of :=
*/
func main() {

	var a int =1 //等价  a:=1
	var v1 int = 10 //正确的使用方式1
	var v2 = 10 //正确的使用方式2，编译器可以自动推导出v2的类型
	v3 := 10 // 正确的使用方式3，编译器可以自动推导出v3的类型
	fmt.Println(a, v1, v2, v3)

}
