package main

import "fmt"

/*//定义匿名函数 并且调用
funAdd:=func(a,b int)int{
	return a+b
}
r:=funAdd(11,22)
fmt.Println("a+b=",r)
//定义 +调用匿名函数 一起
r=func(a,b int)int{
	return a-b
}(11,2)
fmt.Println("a+b=",r)
//Go闭包 通过函数创建匿名函数 并且返回函数*/

func createFunc() (func(aa, bb, cc int) int) {
	return func(aa, bb, cc int) int {
		return aa + bb + cc
	}
}

func main() {
	add := createFunc()
	addNum := add(1, 2, 3)
	fmt.Println("addNum:", addNum)
}

