package main

import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// 定义结构体一定要加 type,type和C/C++的 typedef 类似也可以 起别名
type Data struct{
	name string
	age  int
}
func  main() {
	data:=Data{"a",1}
	fmt.Println(data)
}
