package main

import (
	"fmt"
)

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// 关于浮点数的操作  浮点数 自动推导 是float64即C语言中的double 不能直接和fload32转换 要进行强制转换
func main() {

	var fvalue1 float32
	fvalue1 = 12
	fvalue2 := 12.0 // 如果不加小数点，fvalue2会被推导为整型而不是浮点型
	fmt.Println(fvalue1, fvalue2)

}
