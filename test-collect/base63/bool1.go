package main

import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
//go中的布尔类型 只能用 false  true != ==不能进行强制转换
//Go语言中的布尔类型与其他语言基本一致，关键字也为bool，可赋值为预定义的true和
//false
func main() {

	var v1 bool
	v1 = true
	v2 := (1 == 2) // v2也会被推导为bool类型
	fmt.Println(v1, v2)

	//var b bool
	//b = 1 // 编译错误
	//b = bool(1) // 编译错误
}
