package main

import "fmt"

/*myMap = map[string] Info{
     "1234": PersonInfo{"1", "Jack", "Room 101,..."},
}*/

// https://www.cnblogs.com/sevenyuan/p/6382940.html
//可以使用Go语言内置的函数make()来创建一个新map。下面的这个例子创建了一个键
// 类型为string、值类型为PersonInfo的map
type Info struct{
	name string
	age  int8
}
func main() {
	var infoMap map[string] Info
	infoMap=make(map[string] Info)
	infoMap["s1"]= Info{"ydw",11}
	infoMap["s2"]=Info{"xxx",22}
	fmt.Println(infoMap)
	/////如果sone 没有查找到那么返回值应该是nil 实际上我们只需要判断 ok是否是 true or false 即可判断元素是否查找到
	sone,ok:=infoMap["s1"]
	if ok {
		fmt.Println("s1 student info exists!",sone.name,":",sone.age)
	}else{
		fmt.Println("s1 student info not exists!")
	}

	/////删除一个map用
	delete(infoMap,"s2")
	fmt.Println(infoMap)
}
