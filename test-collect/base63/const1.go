package main

// https://www.cnblogs.com/sevenyuan/p/6382940.html
// 通过const关键字，你可以给字面常量指定一个友好的名字
// Go的常量定义可以限定常量类型，但不是必需的。如果定义常量时没有指定类型，那么它
// 与字面常量一样，是无类型常量。
func main() {
	const Pi float64 = 3.14159265358979323846
	const zero = 0.0             // 无类型浮点常量
	const (
		size int64 = 1024
		eof = -1                // 无类型整型常量
	)
	const u, v float32 = 0, 3    // u = 0.0, v = 3.0，常量的多重赋值
	const a, b, c = 3, 4, "foo"
	const mask = 1 << 3
	//const Home = os.GetEnv("HOME") //由于常量的赋值是一个编译期行为， 所以右值不能出现任何需要运行期才能得出结果的表达 式，比如试图以如下方式定义常量就会导致错误
}
