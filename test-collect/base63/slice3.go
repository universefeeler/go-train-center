package main

import "fmt"

// https://www.cnblogs.com/sevenyuan/p/6382940.html
func main() {

	mySlice1 := make([]int, 5) //创建一个初始元素个数为5的数组切片，元素初始值为0：
	fmt.Println(mySlice1)
	mySlice2 := make([]int, 5, 10) // 创建一个初始元素个数为5的数组切片，元素初始值为0，并预留10个元素的存储空间
	fmt.Println(mySlice2)
	mySlice3 := []int{1, 2, 3, 4, 5} // 直接创建并初始化包含5个元素的数组切片：
	fmt.Println(mySlice3)
	td1:=make([]int,5,20)


	fmt.Println(cap(td1)) // ap()函数返回的是数组切片分配的空间大小
	fmt.Println(len(td1)) // 而len()函数返回的是 数组切片中当前所存储的元素个数
	td1 = append(td1,1,2,3,4,56,6,7) // 为数组切片动态增加元素
	fmt.Println(td1)


	slice1 := []int{1, 2, 3, 4, 5}
	slice2 := []int{5, 4, 3}
	copy(slice2, slice1) // 只会复制slice1的前3个元素到slice2中
	copy(slice1, slice2) // 只会复制slice2的3个元素到slice1的前3个位置
	old:=[]int{1,2,3,4,5,6}
	newSlice:=[]int{1,3,3}
	copy(old,newSlice)
	fmt.Println(old)
}
